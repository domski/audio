class Scheduler {

	constructor(audioContex) {
		this.audioContext = audioContext;
		this.realArray = [0,0.4,0.4,1,1,1,0.3,0.7,0.6,0.5,0.9,0.8];
		this.outputNode = this.audioContext.createGain();
	}

	set realWave(realArr) {
		this.realArray = realArr;
	}

	get realWave() {
		return this.realArray;
	}

	/**
	 * Remember noteLength is in SECONDS
	 */
	scheduleNote(beatNumber, time, noteLength = 0.15, freq = null) {
		let currentNoteStartTime = time;

		// 
		var real = new Float32Array(this.realArray);
		var imag = new Float32Array(this.realArray);
		var hornTable = audioContext.createPeriodicWave(real, imag);

		// create an oscillator
		var osc = this.audioContext.createOscillator();

		osc.setPeriodicWave(hornTable);

		osc.connect(this.outputNode);
		this.outputNode.connect( this.audioContext.destination );

		let frequencies = [160, 220, 220, 320, 220, 220, 440, 320];

		osc.frequency.value = freq || frequencies[beatNumber % frequencies.length];

		osc.start( time );
		osc.stop( time + noteLength );
	}

	nextNote() {
	  // Advance current note and time by a 16th note...
	  var secondsPerBeat = 60.0 / tempo;	// picks up the CURRENT tempo value!
	  nextNoteTime += 0.25 * secondsPerBeat;	// Add 1/4 of quarter-note beat length to time

	  current16thNote++;	// Advance the beat number, wrap to zero
	  if (current16thNote == 16) {
	    current16thNote = 0;
	  }
	}

	schedule(sequencer) {
		let scheduleAheadTime = 100;

		let nextNote = sequencer.getNextNote();

		while (nextNoteTime < this.audioContext.currentTime + scheduleAheadTime ) {
		  scheduleNote( current16thNote, nextNoteTime );
		  nextNote();
		}
	}
}


let highestClip = 0;

function Canvas( element )
{
	this.element = element;

	this.context = this.element.getContext('2d');
	this.ctx = this.context;

	this.width = this.element.width;
	this.height = this.element.height;

	this.ctx.fillStyle = 'rgb(100, 100, 100)';
	this.ctx.lineWidth = 2;
	this.ctx.strokeStyle = 'rgb(140, 250, 240)';

	this.visualiseFreq = function( arrayBuffer )
	{
		// visualiseFunction( analyser, arrayBuffer );
		// var drawVisual = window.requestAnimationFrame( visualiseFunction.bind(this, analyser, arrayBuffer) );
		// var drawVisual = window.requestAnimationFrame( visualiseFunction.bind(this) );

		var ctx = this.ctx;
		ctx.clearRect(0, 0, this.width, this.height);
		ctx.fillStyle = 'rgb(20, 20, 20)';
		var sliceWidth = this.width * 1.0 / arrayBuffer.length;
		ctx.fillRect(0, 0, this.width, this.height);

		ctx.beginPath();
		var x = 0;
		for(var i = 0; i < arrayBuffer.length; i++) {
        var v = arrayBuffer[i] / 128.0;
        var y = v * this.height / 2;
        y *= -1;

        if(i === 0) {
          ctx.moveTo(x, y);
        } else {
          ctx.lineTo(x, y);
		  }

        x += sliceWidth;
      }

      // console.log(Math.max.apply(Math, arrayBuffer), Math.min.apply(Math, arrayBuffer));
   	ctx.lineTo(this.width, this.height/2);
      ctx.stroke();

	}.bind(this);

	this.visualiseWave = function( arrayBuffer )
	{
		// visualiseFunction( analyser, arrayBuffer );
		// var drawVisual = window.requestAnimationFrame( visualiseFunction.bind(this, analyser, arrayBuffer) );
		// var drawVisual = window.requestAnimationFrame( visualiseFunction.bind(this) );

		var ctx = this.ctx;
		ctx.clearRect(0, 0, this.width, this.height);
		ctx.fillStyle = 'rgb(20, 20, 20)';
		var sliceWidth = this.width * 1.0 / arrayBuffer.length;
		ctx.fillRect(0, 0, this.width, this.height);

		const clipping = [];

		ctx.beginPath();
		var x = 0;
		for(var i = 0; i < arrayBuffer.length; i++) {
        var v = arrayBuffer[i];
        var y = v * this.height / 2;
        // y *= -1;
        y += this.height / 2;

        if(v > 1) {
        	 if(v > highestClip) {
        	 	highestClip = v;
        	 	// console.log(highestClip);
        	 }
        	 clipping.push(x);
        }

        if(i === 0) {
          ctx.moveTo(x, y);
        } else {
          ctx.lineTo(x, y);
		  }

        x += sliceWidth;
      }

      // console.log(Math.max.apply(Math, arrayBuffer), Math.min.apply(Math, arrayBuffer));
   	ctx.lineTo(this.width, this.height/2);
      ctx.stroke();

      ctx.fillStyle = '#FF4500';
      clipping.forEach(x => {
      	ctx.fillRect(x, 0, 1, this.height);
      });

	}.bind(this);
}
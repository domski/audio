/*
 * Audio analyser
 *
 * @param element related DOM element
 */
class AudioAnalyser {
	constructor(audioContext) {
		this.audioContext = audioContext;
		this.sourceBuffer = null;
		this.output = null;

		this.analyserNode = audioContext.createAnalyser();
		this.analyserNode.fftSize = 2048;
	}

	connectBuffer(sourceBuffer) {

	}

	// set buffer()???
	// if( undefined != typeof sourceBuffer ) this.setBuffer( sourceBuffer );

	render() {
		// this.output
	}

	visualiseFrequencies( canvas ) {
		// @NOTE: This creates the array evey time. should try to refactor this?
		let frequencyArray = new Float32Array(this.analyserNode.frequencyBinCount);

		this.analyserNode.getFloatFrequencyData( frequencyArray );

		canvas.visualiseFreq( frequencyArray );

		var drawVisuals = window.requestAnimationFrame( this.visualiseFrequencies.bind(this, canvas) );
	}

	visualiseWaveform( canvas ) {
		let waveformArray = new Float32Array(this.analyserNode.frequencyBinCount);

		this.analyserNode.getFloatTimeDomainData( waveformArray );

		canvas.visualiseWave( waveformArray );

		var drawVisuals = window.requestAnimationFrame( this.visualiseWaveform.bind(this, canvas) );
	}

}
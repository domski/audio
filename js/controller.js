/*
 * Creates a controller which allows for controling values with a slider
 *
 * @param context audio context
 * @param element related DOM element, type=range
 */
function SliderController( context, element )
{
	this.context = context;
	this.element = element;

	this.slider = null;
	this.valueOutput = null;
	this.onInput = null;

	this.slider = this.element.getElementsByClassName('controller-value')[0];
	this.valueOutput = this.element.getElementsByClassName('value-output')[0];

	this.slider.addEventListener('input', function(e) {
		if(typeof this.onInput == 'function') {
			this.onInput(this.slider.value);
		}
		if( this.valueOutput !== undefined ) {
			this.valueOutput.innerHTML = this.slider.value;
		}
	}.bind(this), false);

	return this;
}
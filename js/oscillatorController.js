/*
 * creates an oscillator controller from a slider
 *
 * @param context audio context
 * @param element related DOM element
 */
function OscillatorController( context, element )
{
	this.context = context;
	this.element = element;

	// this.tracking = false;
	this.play = false;
	this.started = false;

	this.slider = this.element.getElementsByClassName('osc__slider')[0];
	this.toggle = this.element.getElementsByClassName('osc__toggle')[0];
	this.bang = this.element.querySelectorAll('.osc__bang');

	this.oscillator = this.context.createOscillator();
	this.tuner = this.oscillator.detune;

	// this.oscillator.connect(this.context.destination);

	// this.slider.addEventListener('mousedown', function(e) {
	// 	this.tracking = true;
	// }.bind(this), false);

	this.slider.addEventListener('input', function(e) {
		// if( this.tracking ) {
			this.tuner.value = this.slider.value;
		// }
	}.bind(this), false);

	// this.slider.addEventListener('mouseup', function(e) {
	// 	this.tracking = false;
	// }.bind(this), false);

	this.toggle.addEventListener('click', function(e) {
		this.play = this.play ? false : true;
		if( this.play )
		{
			console.log('connect');
			this.oscillator.connect(this.context.destination);

			if( !this.started ) {
				console.log('start');
				this.started = true;
				this.oscillator.start();
			}
		}
		else
		{
			console.log('disconnect');
			this.oscillator.disconnect(this.context.destination);
		}
	}.bind(this));

	var self = this;

	[].forEach.call(
		self.bang,
		function(element) {
			console.log(element);
			element.addEventListener('click', function(e) {
				if( !self.play )
				{
					console.log('connect');
					self.oscillator.connect(self.context.destination);

					if( !self.started ) {
						console.log('start');
						self.started = true;
						self.oscillator.start();
						self.play = true;
					}

					if(self.bangTimeout) {
						window.clearTimeout(self.bangTimeout);
					}

					self.bangTimeout = window.setTimeout(function() {
						console.log('disconnect');
						self.oscillator.disconnect(self.context.destination);
						self.play = false;
					}, element.dataset.duration);
				}
			}.bind(self));
		}
	)
}
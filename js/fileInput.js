/*
 * Allows file loading, primarily for audio
 *
 * @param element related DOM element
 */
function FileInput( context, element )
{
	this.context = context;
	this.input = element.querySelector('input[type=file]');
	this.wrapper = element.querySelector('.file-input');

	this.callbackSuccess = null;
	this.callbackFailure = null;

	this.then = function( callbackSuccess, callbackFailure ) {
		if(callbackSuccess && typeof callbackSuccess == 'function') this.callbackSuccess = callbackSuccess;
		if(callbackFailure && typeof callbackFailure == 'function') this.callbackFailure = callbackFailure;
	};

	this.input.addEventListener('change', function(e) {

		console.log();
		var input = this.input;
		var files = input.files;
		var fileUrl = URL.createObjectURL(files[0]);

		loadAudioFile( this.context, fileUrl ).then(
			function( sourceBuffer ) {
				if(this.callbackSuccess) {
					this.callbackSuccess( sourceBuffer );
				}
			}.bind(this),

			function( failDump ) {
				if(this.callbackFailure) {
					this.callbackFailure( dump );
				}
			}.bind(this)
		);

	}.bind(this), false);

	this.wrapper.addEventListener('dragover', function(e) {
		var classList = this.className.split(' ');
		if( classList.indexOf('draggingOver') == -1 ) {
			classList.push('draggingOver');
			this.className = classList.join(' ');
		}
	}, false);
	this.wrapper.addEventListener('dragleave', function(e) {
		var classList = this.className.split(' ');
		classList.splice( classList.indexOf('draggingOver') );
		this.className = classList.join(' ');
	}, false);

	// this.input.addEventListener('dragover', function(e) {
	// 	this.className = 'draggingOver';
	// }, false);
	// this.input.addEventListener('dragleave', function(e) {
	// 	this.className = '';
	// }, false);
}
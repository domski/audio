/*
 * Provides a "promise" with `.then()`
 *
 * @param context audio context
 * @param url url to the audio file to be loaded, can be blob
 *
 * @return loadAudioFile ???
 */
function loadAudioFile( context, url )
{
	this.context = context;

	var request = new XMLHttpRequest();
	request.open('GET', url, true);
	request.responseType = 'arraybuffer';

	this.callbackSuccess = null;
	this.callbackFailure = null;

	/*
	 * Provides "promises" with success and failure callbacks
	 *
	 * @param callbackSuccess function to be executed upon a successful file load
	 * @param callbackFailure function to be executed when file load fails
	 */
	this.then = function( callbackSuccess, callbackFailure ) {
		if(callbackSuccess && typeof callbackSuccess == 'function') this.callbackSuccess = callbackSuccess;
		if(callbackFailure && typeof callbackFailure == 'function') this.callbackFailure = callbackFailure;
	};

	// Decode asynchronously
	request.onload = function() {
		var audioContext = this.context;

		audioContext.decodeAudioData( request.response, function( buffer ) {
			window.sourceBuffer = sourceBuffer = this.context.createBufferSource();
			sourceBuffer.buffer = buffer;
			sourceBuffer.fileUrl = url;

			if(this.callbackSuccess) {
				this.callbackSuccess(sourceBuffer);
			}

		}, function( dump ){
			if(this.callbackFailure) {
				this.callbackFailure( dump );
			}
		});
	}.bind(this);

	request.send();

	return this;
}
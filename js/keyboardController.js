function frequencyFromNote(note) {
	return 440 * Math.pow(2, (note - 69) / 12);
}

const keyboardKeys = new Map();
const notes = {
	65: 60,
	87: 61,
	83: 62,
	69: 63,
	68: 64,
	70: 65,
	84: 66,
	71: 67,
	89: 68,
	72: 69,
	85: 70,
	74: 71,
	75: 72
};

// https://en.wikipedia.org/wiki/Observer_pattern
function emitEvent(object, eventType, eventData) {
    const callbacks = object.callbacks[eventType];

    if(callbacks) {
        for(let key of Object.keys(callbacks)) {
            callbacks[key].call(object, eventData);
        }
    }
}

function onKeydown(event) {
	const note = notes[event.keyCode];

	if(note) {
		if(!keyboardKeys.has(note)) {
			const osc = new Oscillator(this.audioCtx);
            const frequency = frequencyFromNote(note);

			osc.start(frequency);
			keyboardKeys.set(note, osc);
			console.log(`Note on: [${event.keyCode}], ${keyboardKeys.size}`);

            emitEvent(this, 'note', {
                note,
                frequency,
                time: this.audioCtx.currentTime
            });
		}
	}
}

function onKeyup(event) {
	const note = notes[event.keyCode];

	// @TODO: Somehow notes still get stuck
	if(note) {
		const osc = keyboardKeys.get(note);
		osc.stop();
		keyboardKeys.delete(note);
		console.log(`Note off: [${event.keyCode}], ${keyboardKeys.size}`);
	}
}

class KeyboardController {
	constructor(audioContext) {
		this.audioCtx = audioContext;
        this.callbacks = {};

        // @TODO: Private
        this._callbackId = 0;
	}

	init() {
		// @TODO: can't be unbound, lost reference at this point:
		document.body.addEventListener('keydown', onKeydown.bind(this));
		document.body.addEventListener('keyup', onKeyup.bind(this));
	}

	on(eventType, callback) {
        // return the function to unregister?
        const thisCallbackId = this._callbackId++;
        this.callbacks[eventType] = this.callbacks[eventType] || {};
        this.callbacks[eventType][thisCallbackId] = callback;

        return (function off() {
            delete this.callbacks[eventType][thisCallbackId];
        }).bind(this);
	}
}

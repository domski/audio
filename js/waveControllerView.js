function parseHTML(str) {
	let contextRange = document.createRange();
	contextRange.setStart(document.body, 0);

	return contextRange.createContextualFragment(str);
}

class WaveControllerView {
	constructor(element) {
		this.element = element;

		function findAncestor (el, cls) {
			while ((el = el.parentElement) && !el.classList.contains(cls));
			return el;
		}

		this.element.addEventListener('input', event => {
			let parent = findAncestor(event.target, 'js-wave-modifier-input');

			if(parent) {
				[].slice.apply(parent.querySelectorAll('input')).forEach(input => {
					input.value = event.target.value;
				})
			}
		});
	}

	generateInput() {
		return parseHTML(
		  '<span class="js-wave-modifier-input">' +
		    '<input type="number" step="0.1"></input>' +
		    '<input type="range" min="0" max="1" step="0.1"></input>' +
		  '</span>' +
		'').firstChild;
	}

	get waveFormData() {
		return [].slice.apply(this.element.querySelectorAll('input[type="number"]')).map(input => input.value);
	}

	render(waveDataArray) {
		let inputsContainer = this.element.querySelector('.js-inputs');
		inputsContainer.innerHTML = '';

		for(let waveValue of waveDataArray) {
			let waveInput = this.generateInput();
			inputsContainer.appendChild(waveInput);

			Array.from(waveInput.querySelectorAll('input')).forEach(input => input.value = waveValue);
		}
	}
}
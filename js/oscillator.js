class Oscillator {
	constructor(audioContext) {
		this.audioContext = audioContext;
	}

	init() {
		
	}

	start(frequency, velocity = 1) {
		if(this.osc) {
			console.warn('Cannot call start more than once, oscillator already playing or has stopped.');
			return;
		}
		
		this.osc = this.audioContext.createOscillator();
		this.gainNode = this.audioContext.createGain();
		this.osc.connect(this.gainNode);

		if(frequency) {
			this.osc.frequency.value = frequency;
		}

		if(window.periodicWaveAnalyser) {
			this.gainNode.connect( window.periodicWaveAnalyser.analyserNode );
		}
		this.gainNode.connect( this.audioContext.destination );

		this.gainNode.gain.value = 0;
		this.gainNode.gain.linearRampToValueAtTime(velocity, this.audioContext.currentTime + 0.01);

		this.osc.start();
	}

	stop() {
		return new Promise((resolve, reject) => {
			// this.gainNode.gain.linearRampToValueAtTime(0, this.audioContext.currentTime + 2);
			// @NOTE: Can't exponential to 0
			this.gainNode.gain.exponentialRampToValueAtTime(0.01, this.audioContext.currentTime + 1);
			this.osc.stop(this.audioContext.currentTime + 1);

			this.osc.addEventListener('ended', () => {
				this.osc.disconnect();
				this.gainNode.disconnect();
				resolve();
			});
		});
	}
}
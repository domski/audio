class Sequencer {

	constructor(audioContext) {
		this.audioContext = audioContext;
		this.viewElement = null;
		this.tempo = 0;
		this.taps = [];

		this.audioContextStartTime = this.audioContext.currentTime;
		this.sequenceTime = 0.0;
		this.isPlaying = false;

		this.notesQueue = [];
		this.nextNoteIndex = 0;
	}

	setBPM(bpm) {
		this.tempo = bpm;
		this.render();
	}

	// how beats per second
	setTempo(tempo) {
		this.tempo = 60 / tempo;
		this.render();
	}

	reset() {
		this.sequenceTime = 0.0;
	}

	start() {
		this.reset();
		this.isPlaying = true;
		this.audioContextStartTime = this.audioContext.currentTime;
	}

	stop() {
		this.reset();
		this.isPlaying = false;
	}

	get sequenceLength() {
		const lastNote = this.notesQueue[this.notesQueue.length - 1];
		// return (lastNote.start + lastNote.duration) * (this.tempo / 60);
		return this.notesQueue.length + 1;
	}

	get timeInSequence() {
		if(!this.isPlaying) {
			return 0.0;
		}

		return this.sequenceTime = (this.audioContext.currentTime - this.audioContextStartTime) % (this.sequenceLength);
	}

	getNextNote() {
		if(!this.isPlaying) {
			return;
		}

		let nextNote = this.notesQueue[this.nextNoteIndex];

		this.nextNoteIndex++;
		this.nextNoteIndex = this.nextNoteIndex % this.notesQueue.length;

		return nextNote;
		// while(this.notesQueue.length && notesQueue[0] < this.audioContext.currentTime) {

		// }
	}

	tapTempo() {
		// add tap
		this.taps.push(this.audioContext.currentTime);

		// keep up to 4 taps
		this.taps = this.taps.slice(-4);

		// calculate tempo
		if(this.taps.length === 4) {
			// Calculate average time between taps
			// We ignore the first tap pretty much
			let newTempo = this.taps.reduce((prev, curr, index, arr) => {
				return prev + (curr - arr[Math.max(0, index - 1)]);
			}, 0);

			newTempo /= (this.taps.length - 1);

			this.setTempo(newTempo);
		}
	}

	render() {
		this.viewElement.innerText = this.tempo.toFixed(0);
	}
}
window.request

var audioContext;
window.addEventListener('load', init, false);

// soundBuffer = null;
sourceBuffer = null;

function init()
{
	try {
		window.AudioContext = window.AudioContext || window.webkitAudioContext;
		audioContext = new AudioContext();
	}
	catch(e)
	{
		alert('Web Audio API is not supported in this browser');
		return false;
	}

	var oscillatorControllers = document.getElementsByClassName('mod-osc');

	for (var i = 0; i < oscillatorControllers.length; i++) {
		var osc = oscillatorControllers[i];

		new OscillatorController( audioContext, osc );
	};

	oscillator = audioContext.createOscillator();
	oscillator.connect(audioContext.destination);

	tuner = oscillator.detune;

	canvasFrequency = new Canvas( document.getElementById('canvas-frequency') );
	canvasWaveform = new Canvas( document.getElementById('canvas-waveform') );

	var fileInputs = document.getElementsByClassName('mod-fileInput');
	for (var i = 0; i < fileInputs.length; i++) {
		var element = fileInputs[i];

		var file = new FileInput( audioContext, element );

		file.then( function() {
			var audioPlayer = window.audioPlayer = document.getElementById('audio_player');
			audioPlayer.src = sourceBuffer.fileUrl;
			// audioPlayer.play();

			// audioPlayerSource = window.audioPlayerSource = audioContext.createMediaElementSource( audioPlayer );

			var gainNode = window.gainNode = audioContext.createGain();

			var sourceTuner = sourceBuffer.detune;
			var playbackRate = sourceBuffer.playbackRate;

			var detuneController = new SliderController( audioContext, document.getElementById('ctrl-detune') );
			detuneController.onInput = function( value ) {
				sourceTuner.value = value;
			};
			var playbackRateController = new SliderController( audioContext, document.getElementById('ctrl-playback') );
			playbackRateController.onInput = function( value ) {
				playbackRate.value = value;
			}
			var levelController = new SliderController( audioContext, document.getElementById('ctrl-level') );
			levelController.onInput = function( value ) {
				gainNode.gain.value = value;
			};

			var filterNode = window.filter = audioContext.createBiquadFilter();

			sourceBuffer.connect(gainNode);
			gainNode.connect(filterNode);

			filterNode.connect( this.context.destination );

			var frequencyCutoffController = new SliderController( audioContext, document.getElementById('ctrl-frequency') );
			frequencyCutoffController.onInput = function( value ) {
				// STOLEN from here http://www.html5rocks.com/en/tutorials/webaudio/intro/
				// Clamp the frequency between the minimum value (40 Hz) and half of the
				// sampling rate.
				var minValue = 40;
				var maxValue = audioContext.sampleRate / 2;
				// Logarithm (base 2) to compute how many octaves fall in the range.
				var numberOfOctaves = Math.log(maxValue / minValue) / Math.LN2;
				// Compute a multiplier from 0 to 1 based on an exponential scale.
				var multiplier = Math.pow(2, numberOfOctaves * (value - 1.0));
				// Get back to the frequency value between min and max.
				// this.filter.frequency.value = maxValue * multiplier;
				filterNode.frequency.value = maxValue * multiplier;;
			}

			var qualityController = new SliderController( audioContext, document.getElementById('ctrl-quality') );
			qualityController.onInput = function( value ) {
				// STOLEN from here http://www.html5rocks.com/en/tutorials/webaudio/intro/
				filterNode.Q.value = value * 30;
			}

			var gainController = new SliderController( audioContext, document.getElementById('ctrl-gain') );
			gainController.onInput = function( value ) {
				// STOLEN from here http://www.html5rocks.com/en/tutorials/webaudio/intro/
				filterNode.gain.value = value * 30;
			}

			var filterTypeSelect = document.getElementById('ctrl-type');
			filterTypeSelect.addEventListener('change', function(e) {
				filterNode.type = this.value;
			});

			let analyser = new AudioAnalyser(audioContext);
			filterNode.connect( analyser.analyserNode );

			analyser.visualiseFrequencies( canvasFrequency );
			analyser.visualiseWaveform( canvasWaveform );

			// sourceBuffer.connect( this.context.destination );
			sourceBuffer.start();

		}, function() {
			console.log(failDump);
		});
	};

	let scheduler = new Scheduler(audioContext);

	let waveComponent = new WaveControllerView(document.querySelector('.js-wave-component'));
	// TODO: should init; should not pass wave to render, give the wave to it if anything
	waveComponent.render(scheduler.realWave);

	window.periodicWaveAnalyser = new AudioAnalyser(audioContext);
	let periodicWaveAnaliserCanvas = document.querySelector('#js-periodic-wave-analyser-canvas');

	let periodicWaveCanvas = new Canvas(periodicWaveAnaliserCanvas);
	window.periodicWaveAnalyser.visualiseWaveform(periodicWaveCanvas);
	scheduler.outputNode.connect(window.periodicWaveAnalyser.analyserNode);

	waveComponent.element.addEventListener('input', event => {
		console.log(waveComponent.waveFormData);;
	});

	let sequencer = window.sequencer = new Sequencer(audioContext);
	let tapButton = document.querySelector('.js-tap-tempo');
	sequencer.viewElement = document.querySelector('.js-sequencer-tempo');
	sequencer.setBPM(60);
	sequencer.notesQueue = [ // this is in BEATS
		// Bouree In E Minor
		{start: 0, duration: 0.15, freq: 329},
		{start: 0.2, duration: 0.15, freq: 369},
		{start: 0.4, duration: 0.15, freq: 392},

		{start: 0.8, duration: 0.15, freq: 369},
		{start: 1.0, duration: 0.15, freq: 329},
		{start: 1.2, duration: 0.15, freq: 311},

		{start: 1.6, duration: 0.15, freq: 329},
		{start: 1.8, duration: 0.15, freq: 369},
		{start: 2.0, duration: 0.15, freq: 246},

		{start: 2.4, duration: 0.15, freq: 277},
		{start: 2.6, duration: 0.15, freq: 311},
		{start: 2.8, duration: 0.15, freq: 329},

		{start: 3.2, duration: 0.15, freq: 293},
		{start: 3.4, duration: 0.15, freq: 261},
		{start: 3.6, duration: 0.15, freq: 246},

		{start: 4.0, duration: 0.15, freq: 220},
		{start: 4.2, duration: 0.15, freq: 196},
		{start: 4.4, duration: 0.15, freq: 185},

		{start: 4.8, duration: 0.15, freq: 196},
		{start: 5.0, duration: 0.15, freq: 220},
		{start: 5.2, duration: 0.15, freq: 246},

		{start: 5.4, duration: 0.15, freq: 220},
		{start: 5.6, duration: 0.15, freq: 196},

		{start: 5.8, duration: 0.15, freq: 185},

		{start: 6.0, duration: 1.0, freq: 164},
		{start: 6.0, duration: 1.0, freq: 196},
		{start: 6.0, duration: 1.0, freq: 246},
	];

	// let sequence = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1];
	let currentNote = 0;
	let currentTime = audioContext.currentTime;
	let spacing = 1; // 1 second

	while(currentNote < sequencer.notesQueue.length) {
		let note = sequencer.notesQueue[currentNote];
		// scheduler.scheduleNote(currentNote, note.start, note.duration, note.freq);

		currentTime += spacing;
		currentNote++;
	}

	sequencer.start();

	tapButton.addEventListener('click', function() {
		sequencer.tapTempo();
	});

	function scheduleNextNote(sequencer, scheduler) {

	}

	scheduleNextNote();

	let sequenceView = document.querySelector('.js-sequence-view');
	function renderSequenceTimeline() {
		sequenceView.querySelector('.js-sequence-time').style.left = (sequencer.timeInSequence / sequencer.sequenceLength) * 100 + '%';

		window.requestAnimationFrame( renderSequenceTimeline );
	}

	// Render timeline progress
	// window.requestAnimationFrame( renderSequenceTimeline );

	const midiController = new MIDIController(audioContext);
	midiController.init();

	window.keyboardController = new KeyboardController(audioContext);
	keyboardController.init();

    recorder = new SequenceRecorder(audioContext);

    let recordedSequence;
    const lblRecordStatus = document.querySelector('.js-record-status');
    const btnStart = document.querySelector('.js-start-recording');
    const btnStop = document.querySelector('.js-stop-recording');
    const btnClear = document.querySelector('.js-clear-recording');
    const btnPlay = document.querySelector('.js-play-sequence');
    const sequenceView2 = document.querySelector('.js-sequence-view2');

    function renderSequenceView(sequence) {
        sequenceView2.innerHTML = '';

        for(let note of sequence) {
            sequenceView2.innerHTML += `${note.note} `;
        }
    }

    btnStart.addEventListener('click', function() {
        recorder.startRecording(keyboardController);
        lblRecordStatus.innerText = 'Recording';
    });

    btnStop.addEventListener('click', function() {
        recorder.stopRecording();
        recordedSequence = recorder.sequence;

        lblRecordStatus.innerText = '';
        renderSequenceView(recordedSequence);
    });

    btnClear.addEventListener('click', function() {
        recorder.clearSequence();

        renderSequenceView([]);
    });

    btnPlay.addEventListener('click', function() {
        const startTime = audioContext.currentTime;

        for(let note of recordedSequence) {
            scheduler.scheduleNote(0, startTime + note.time, 0.15, note.frequency);
        }

        sequencer.start();
    });
}

class SequenceRecorder {
	constructor(audioContext) {
		this.audioCtx = audioContext;
	}

	/**
	 * Record output from a controller
	 *
	 * @param {Controller} controller
	 *
	 * @return {boolean}
	 */
	startRecording(controller) {
		this.sequence = [];
        const startTime = this.audioCtx.currentTime;

		this.recordController = controller;

		this.recordControllerOff = controller.on('note', eventData => {
            eventData.time -= startTime;
            this.sequence.push(eventData);
		});
	}

	stopRecording() {
        if(this.recordControllerOff) {
            this.recordControllerOff();
        }
	}

    clearSequence() {
        this.sequence = [];
    }
}

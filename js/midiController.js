function requestMIDI() {
	// request MIDI access
	if (navigator.requestMIDIAccess) {
	    return navigator.requestMIDIAccess({
	        sysex: false // this defaults to 'false' and we won't be covering sysex in this article. 
	    });
	} else {
		return Promise.reject(new Error("No MIDI support in your browser."));
	}
}

function frequencyFromNote(note) {
	return 440 * Math.pow(2, (note - 69) / 12);
}

const keys = new Map();

class MIDIController {
	constructor(audioContext) {
		this.audioCtx = audioContext;
	}

	init() {
		requestMIDI().then(midiAccess => {
			console.log('MIDI Access: ', midiAccess);

			const inputs = midiAccess.inputs.values();

			for(let input = inputs.next(); input && !input.done; input = inputs.next()) {
				console.log(input.value);
				input.value.addEventListener('midimessage', message => {
					// Not sure why but a message of [254] comes in continuously
					if(message.data.length > 1) {
						// cmd 11 = pressure / pedal (0 - 127)
						// cmd 9 = noteOn
						// cmd 8 = noteOff
						const cmd = message.data[0] >> 4;
						const channel = message.data[0] & 0xf;
						const type = message.data[0] & 0xf0;
						const note = message.data[1];
						const velocity = message.data[2]; // 0 - 127

						console.log(`${cmd} ${channel} ${type} ${message.data}`);

						if(cmd === 9) {
							const osc = new Oscillator(this.audioCtx);
							osc.start(frequencyFromNote(note));
							keys.set(note, osc);
						} else if(cmd === 8) {
							const osc = keys.get(note);
							osc.stop();
							keys.delete(note);
						}
					}
				});
			} 

		}).catch(error => {
			console.error(error);
		});
	}
}
# Tasks

* for MYO
	* sequencer
		* snap to set beat
	* Myo input to control nodes
* Procedural sound
	* based on movement speed, i.e. Car engine
	* Generate robotic speech sounds based on provided text
* Beat matching
	* Beat recognition
		* http://joesul.li/van/beat-detection-using-web-audio/
		* OfflineAudioContext
	* visualisation, correctly synced frequency visualisation
	* beat adjustment, how much to adjust by to meet a certain beat
	* Automatic beat matching suggestions and adjustment
		* any way to store files locally?
	* Appropriate GUI to visualise and adjust the parameters for the algorithms.
	* Try beat matching and mixing two songs, from youtube videos:
		* http://www.youtube.com/watch?v=XxWNx6KbvZY
		* http://www.youtube.com/watch?v=R7z-tA9oLPw
* remain init to main
* some build process?
* translate to ES6
* create sequencer
* add web sockets, i.e. NODE JS
	* figure out the appropriate file structure for NODE project, express, serving files etc.
	* For syncing between clients I imagine

# Refs

http://www.html5rocks.com/en/tutorials/audio/scheduling/
https://github.com/cwilso/metronome
http://stackoverflow.com/questions/30631595/custom-wave-forms-in-web-audio-api